# README #  update: 2015/02/14


### What ###
  個人的にAdobe Photoshop CCのスクリプトを管理しするプロジェクト


### Notifications ###
 - ファイルはネットで見つけたものです。（後々自作してみたい・・・）
 - 選定基準は、「デフォルト機能としてまだない」「私個人が制作時に困った局面で欲しいと思った」という2点です。
 - 各スクリプトの内容は、以下のconfigurationに記載のURLをご覧ください。
 - 日々ベストな状態に、カスタマイズしています。ファイルの変更履歴は、commit履歴をご覧ください。


### System Requirements ###
 - OS X 10.10.3上のAdobe Photoshop CC 2015以外での動作確認はしていません（2015/02/04現在）。


### How do I get set up? ###
"/Applications/Adobe Photoshop CC 2015/Presets/Scriptsスクリプト"以下にファイルを配置、Photoshop起動で使用できます。illustrator起動中の場合は、再起動してください。


### Configuration ###
 - 個人的に追加したファイル
  Distribute_Layer_Spacing_Horizontal.jsx
  http://morris-photographics.com/photoshop/scripts/distribute-horizontally.html


  Distribute_Layer_Spacing_Vertical.jsx
  http://morris-photographics.com/photoshop/scripts/distribute-vertically.html



 - デフォルトでディレクトリにあったもの

  ArtBoards To Files.jsx


  Image Processor.jsx


  ArtBoards To PDF.jsx


  Layer Comps To Files.jsx


  ArtboardExport.inc


  Layer Comps To PDF.jsx


  Conditional Mode Change.jsx


  Lens Correct.jsx


  ContactSheetII.jsx


  Load DICOM.jsx


  Delete All Empty Layers.jsx


  Load Files into Stack.jsx



  Merge To HDR.jsx


  Photomerge.jsx


  Event Scripts Only


  Export Layers To Files.jsx


  Script Events Manager.jsx


  ExportColorLookupTables.jsx


  Stack Scripts Only


  Fit Image.jsx


  Statistics.jsx


  Flatten All Layer Effects.jsx


  generate.jsx


  Flatten All Masks.jsx

### Contribution ###
 おすすめのスクリプトがあったら教えて下さい！プルリク歓迎です！